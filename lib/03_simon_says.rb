def echo(string)
  return string
end

def shout(string)
  string.upcase
end

# def repeat(string, num)
#   arr = []
#   until arr.length == num
#     arr << string
#   end
#   arr.join(" ")
# end

def repeat(string, times = 2)
  words = Array.new(times, string)
  words.join(" ")
end

def start_of_word(string, num)
  string[0...num]
end

def first_word(phrase)
  phrase.split.first
end

def titleize(title)
  little_words = ["and", "the", "over"]
  words = title.split
  words.map!.with_index do |word, idx|
    if idx != 0 && little_words.include?(word)
      word.downcase
    else
      word.capitalize
    end
  end
  words.join(" ")
end
