def add(int1, int2)
  int1 + int2
end

def subtract(int1, int2)
  int1 - int2
end

def sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end
